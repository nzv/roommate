package com.isil.roommate;

import com.isil.roommate.entity.ApplicationConfiguration;
import com.isil.roommate.entity.Authority;
import com.isil.roommate.entity.Booking;
import com.isil.roommate.entity.BookingStatus;
import com.isil.roommate.entity.Guest;
import com.isil.roommate.entity.Role;
import com.isil.roommate.entity.Room;
import com.isil.roommate.entity.RoomStatus;
import com.isil.roommate.entity.RoomType;
import com.isil.roommate.entity.User;
import com.isil.roommate.repository.ApplicationConfigurationRepository;
import com.isil.roommate.repository.AuthorityRepository;
import com.isil.roommate.repository.RoleRepository;
import com.isil.roommate.repository.RoomRepository;
import com.isil.roommate.repository.UserRepository;
import com.isil.roommate.service.BookingService;
import com.isil.roommate.service.GuestService;
import com.isil.roommate.service.UserService;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

@SpringBootApplication
public class RoommateApplication {

  /**
   * Método principal que inicia la aplicación Spring Boot.
   *
   * @param args Argumentos de línea de comandos proporcionados al iniciar la aplicación.
   */
  public static void main(String[] args) {
    SpringApplication.run(RoommateApplication.class, args);
  }

  /**
   * Configuración de un bean Thymeleaf para integrar el dialecto de seguridad de Spring.
   *
   * @return Instancia de SpringSecurityDialect para su uso en las plantillas Thymeleaf.
   */
  @Bean
  public SpringSecurityDialect springSecurityDialect() {
    return new SpringSecurityDialect();
  }

  /**
   * Crea un bean CommandLineRunner para cargar datos base en la aplicación.
   *
   * @param authRepo Repositorio de autoridades.
   * @param roleRepo Repositorio de roles.
   * @param userService Servicio de usuarios.
   * @param userRepo Repositorio de usuarios.
   * @param applicationConfigurationRepository Repositorio de configuraciones de la aplicación.
   * @return Instancia de CommandLineRunner para cargar datos base en la aplicación.
   */
  @Bean
  public CommandLineRunner dataLoaderBase(
      AuthorityRepository authRepo,
      RoleRepository roleRepo,
      UserService userService,
      UserRepository userRepo,
      ApplicationConfigurationRepository applicationConfigurationRepository) {
    return args -> {
      LocalDateTime now = LocalDateTime.now();
      List<Authority> authoritiesAdmin = new ArrayList<>();
      if (authRepo.findByName("USUARIO_LISTAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("USUARIO_LISTAR").build());
      }
      if (authRepo.findByName("USUARIO_LISTAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("USUARIO_CREAR").build());
      }
      if (authRepo.findByName("USUARIO_ELIMINAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("USUARIO_ELIMINAR").build());
      }
      if (authRepo.findByName("USUARIO_EDITAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("USUARIO_EDITAR").build());
      }
      if (authRepo.findByName("ROL_LISTAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("ROL_LISTAR").build());
      }
      if (authRepo.findByName("ROL_CREAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("ROL_CREAR").build());
      }
      if (authRepo.findByName("ROL_ELIMINAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("ROL_ELIMINAR").build());
      }
      if (authRepo.findByName("ROL_EDITAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("ROL_EDITAR").build());
      }
      if (authRepo.findByName("HABITACION_LISTAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("HABITACION_LISTAR").build());
      }
      if (authRepo.findByName("HABITACION_CREAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("HABITACION_CREAR").build());
      }
      if (authRepo.findByName("HABITACION_ELIMINAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("HABITACION_ELIMINAR").build());
      }
      if (authRepo.findByName("HABITACION_EDITAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("HABITACION_EDITAR").build());
      }
      if (authRepo.findByName("CONFIGURACION_GLOBAL_LISTAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("CONFIGURACION_GLOBAL_LISTAR").build());
      }
      if (authRepo.findByName("CONFIGURACION_GLOBAL_EDITAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("CONFIGURACION_GLOBAL_EDITAR").build());
      }
      if (authRepo.findByName("RESERVA_LISTAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("RESERVA_LISTAR").build());
      }
      if (authRepo.findByName("RESERVA_CREAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("RESERVA_CREAR").build());
      }
      if (authRepo.findByName("RESERVA_ELIMINAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("RESERVA_ELIMINAR").build());
      }
      if (authRepo.findByName("RESERVA_EDITAR") == null) {
        authoritiesAdmin.add(Authority.builder().name("RESERVA_EDITAR").build());
      }
      if (authoritiesAdmin.size() > 1) {
        authRepo.saveAll(authoritiesAdmin);
      }
      Role roleAdmin = Role.builder().name("ADMINISTRADOR").authorities(authoritiesAdmin).build();
      if (roleRepo.findFirstByName("ADMINISTRADOR") == null) {
        roleRepo.save(roleAdmin);
      }
      if (userRepo.findByUsername("00111111") == null) {
        User userAdmin =
            User.builder().username("00111111").password("foo").role(roleAdmin).build();
        userService.save(userAdmin);
      }
      List<ApplicationConfiguration> applicationConfigurations = new ArrayList<>();
      if (applicationConfigurationRepository.findByConfigKey("maxLoginAttemptsThreshold") == null) {
        applicationConfigurations.add(
            ApplicationConfiguration.builder()
                .description("Número máximo de intentos de inicio de sesión")
                .configKey("maxLoginAttemptsThreshold")
                .value("3")
                .build());
      }
      if (applicationConfigurationRepository.findByConfigKey("lockTimeDurationMinutes") == null) {
        applicationConfigurations.add(
            ApplicationConfiguration.builder()
                .description("Duración del bloqueo de acceso en minutos")
                .configKey("lockTimeDurationMinutes")
                .value("10")
                .build());
      }
      if (applicationConfigurations.size() > 1) {
        applicationConfigurationRepository.saveAll(applicationConfigurations);
      }
    };
  }

  /**
   * Crea un bean CommandLineRunner para cargar datos de prueba en el perfil de desarrollo.
   *
   * @param authorityRepo Repositorio de autoridades.
   * @param roomRepo Repositorio de habitaciones.
   * @param roleRepo Repositorio de roles.
   * @param userService Servicio de usuarios.
   * @param guestService Servicio de huéspedes.
   * @param bookingService Servicio de reservas.
   * @return Instancia de CommandLineRunner para cargar datos de prueba en el perfil de desarrollo.
   */
  @Bean
  @Profile("dev")
  public CommandLineRunner dataLoaderMock(
      AuthorityRepository authorityRepo,
      RoomRepository roomRepo,
      RoleRepository roleRepo,
      UserService userService,
      GuestService guestService,
      BookingService bookingService) {
    return args -> {
      roleRepo.saveAll(
          List.of(
              Role.builder()
                  .name("USER_MANAGEMENT")
                  .authorities(authorityRepo.findByNameStartsWith("USUARIO_"))
                  .build(),
              Role.builder()
                  .name("ROLE_MANAGEMENT")
                  .authorities(authorityRepo.findByNameStartsWith("ROL_"))
                  .build()));
      userService.save(
          User.builder()
              .username("00111112")
              .password("foo")
              .role(roleRepo.findFirstByName("USER_MANAGEMENT"))
              .build());
      userService.save(
          User.builder()
              .username("00111113")
              .password("foo")
              .role(roleRepo.findFirstByName("ROLE_MANAGEMENT"))
              .build());
      userService.save(
          User.builder()
              .username("00111114")
              .password("foo")
              .enabled(false)
              .role(roleRepo.findFirstByName("ADMINISTRADOR"))
              .build());
      List<Room> rooms =
          List.of(
              Room.builder()
                  .roomNumber(101)
                  .type(RoomType.SINGLE)
                  .pricePerNight(BigDecimal.valueOf(100.00))
                  .capacity(1)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusMonths(1))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(102)
                  .type(RoomType.DOUBLE)
                  .pricePerNight(BigDecimal.valueOf(120.00))
                  .capacity(2)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusDays(20))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(103)
                  .type(RoomType.SINGLE)
                  .pricePerNight(BigDecimal.valueOf(110.00))
                  .capacity(1)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusDays(15))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(104)
                  .type(RoomType.DOUBLE)
                  .pricePerNight(BigDecimal.valueOf(130.00))
                  .capacity(2)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusDays(10))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(105)
                  .type(RoomType.SINGLE)
                  .pricePerNight(BigDecimal.valueOf(115.00))
                  .capacity(1)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusDays(5))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(106)
                  .type(RoomType.DOUBLE)
                  .pricePerNight(BigDecimal.valueOf(140.00))
                  .capacity(2)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusDays(3))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(107)
                  .type(RoomType.SINGLE)
                  .pricePerNight(BigDecimal.valueOf(120.00))
                  .capacity(1)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusDays(7))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(108)
                  .type(RoomType.DOUBLE)
                  .pricePerNight(BigDecimal.valueOf(150.00))
                  .capacity(2)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusDays(2))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(109)
                  .type(RoomType.SINGLE)
                  .pricePerNight(BigDecimal.valueOf(125.00))
                  .capacity(1)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusDays(10))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(110)
                  .type(RoomType.DOUBLE)
                  .pricePerNight(BigDecimal.valueOf(160.00))
                  .capacity(2)
                  .floor(1)
                  .lastMaintenanceDate(LocalDate.now().minusDays(4))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(112)
                  .type(RoomType.DOUBLE)
                  .pricePerNight(BigDecimal.valueOf(150.00))
                  .capacity(2)
                  .floor(2)
                  .lastMaintenanceDate(LocalDate.now().minusDays(15))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(113)
                  .type(RoomType.SINGLE)
                  .pricePerNight(BigDecimal.valueOf(120.00))
                  .capacity(1)
                  .floor(3)
                  .lastMaintenanceDate(LocalDate.now().minusDays(7))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(114)
                  .type(RoomType.DOUBLE)
                  .pricePerNight(BigDecimal.valueOf(180.00))
                  .capacity(2)
                  .floor(4)
                  .lastMaintenanceDate(LocalDate.now().minusDays(10))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(115)
                  .type(RoomType.SINGLE)
                  .pricePerNight(BigDecimal.valueOf(110.00))
                  .capacity(1)
                  .floor(5)
                  .lastMaintenanceDate(LocalDate.now().minusDays(5))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(116)
                  .type(RoomType.DOUBLE)
                  .pricePerNight(BigDecimal.valueOf(200.00))
                  .capacity(2)
                  .floor(6)
                  .lastMaintenanceDate(LocalDate.now().minusDays(3))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(117)
                  .type(RoomType.SINGLE)
                  .pricePerNight(BigDecimal.valueOf(130.00))
                  .capacity(1)
                  .floor(7)
                  .lastMaintenanceDate(LocalDate.now().minusDays(8))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(118)
                  .type(RoomType.DOUBLE)
                  .pricePerNight(BigDecimal.valueOf(170.00))
                  .capacity(2)
                  .floor(8)
                  .lastMaintenanceDate(LocalDate.now().minusDays(12))
                  .status(RoomStatus.CLEAN)
                  .build(),
              Room.builder()
                  .roomNumber(119)
                  .type(RoomType.SINGLE)
                  .pricePerNight(BigDecimal.valueOf(125.00))
                  .capacity(1)
                  .floor(9)
                  .lastMaintenanceDate(LocalDate.now().minusDays(6))
                  .status(RoomStatus.CLEAN)
                  .build());
      roomRepo.saveAll(rooms);
      // Save guests
      List<Guest> guests =
          List.of(
              Guest.builder()
                  .name("John Doe")
                  .dni("12345678")
                  .motherLastName("Doe")
                  .fatherLastName("Smith")
                  .phoneNumber("123456789")
                  .email("john.doe@example.com")
                  .build(),
              Guest.builder()
                  .name("Jane Doe")
                  .dni("87654321")
                  .motherLastName("Doe")
                  .fatherLastName("Smith")
                  .phoneNumber("987654321")
                  .email("jane.doe@example.com")
                  .build(),
              Guest.builder()
                  .name("Bob Johnson")
                  .dni("34567890")
                  .motherLastName("Johnson")
                  .fatherLastName("Brown")
                  .phoneNumber("567890123")
                  .email("bob.johnson@example.com")
                  .build(),
              Guest.builder()
                  .name("Alice Smith")
                  .dni("98765432")
                  .motherLastName("Smith")
                  .fatherLastName("Jones")
                  .phoneNumber("876543210")
                  .email("alice.smith@example.com")
                  .build(),
              Guest.builder()
                  .name("Jose Carlos")
                  .dni("91265232")
                  .motherLastName("Flores")
                  .fatherLastName("Ramos")
                  .phoneNumber("812212210")
                  .email("jose.carlos@example.com")
                  .build());
      guestService.saveAll(guests);
      // Save bookings
      List<Booking> bookings =
          List.of(
              Booking.builder()
                  .room(rooms.get(0))
                  .start(LocalDateTime.now().plusDays(1))
                  .end(LocalDateTime.now().plusDays(5))
                  .status(BookingStatus.PENDING)
                  .guest(guests.get(0))
                  .build(),
              Booking.builder()
                  .room(rooms.get(1))
                  .start(LocalDateTime.now().plusDays(3))
                  .end(LocalDateTime.now().plusDays(7))
                  .status(BookingStatus.CONFIRMED)
                  .guest(guests.get(1))
                  .build(),
              Booking.builder()
                  .room(rooms.get(2))
                  .start(LocalDateTime.now().plusDays(2))
                  .end(LocalDateTime.now().plusDays(6))
                  .status(BookingStatus.CANCELLED)
                  .guest(guests.get(2))
                  .build(),
              Booking.builder()
                  .room(rooms.get(3))
                  .start(LocalDateTime.now().plusDays(4))
                  .end(LocalDateTime.now().plusDays(8))
                  .status(BookingStatus.CHECKED_IN)
                  .guest(guests.get(3))
                  .build(),
              Booking.builder()
                  .room(rooms.get(4))
                  .start(LocalDateTime.now().plusDays(1))
                  .end(LocalDateTime.now().plusDays(3))
                  .status(BookingStatus.CHECKED_OUT)
                  .guest(guests.get(4))
                  .build());
      bookingService.saveAll(bookings);
    };
  }
}
