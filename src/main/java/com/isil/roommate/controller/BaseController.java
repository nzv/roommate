package com.isil.roommate.controller;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Clase abstracta BaseController que provee funcionalidades comunes para los controladores en el
 * paquete com.isil.roommate. Incluye la gestión de mensajes internacionalizados, redirección de
 * vistas y limpieza de atributos.
 *
 * @see org.springframework.beans.factory.annotation.Autowired
 * @see org.springframework.web.servlet.mvc.support.RedirectAttributes
 */
public abstract class BaseController {

  /**
   * Redirige a la vista especificada y la agrega como atributo flash.
   *
   * @param viewName Nombre de la vista a la que redirigir.
   * @param red RedirectAttributes para agregar atributos flash.
   * @return La ruta de redirección.
   */
  protected String redirectToView(String viewName, RedirectAttributes red) {
    red.addFlashAttribute("view", viewName);
    return "redirect:/";
  }

  /**
   * Limpia los atributos flash de error, éxito y advertencia en RedirectAttributes.
   *
   * @param red RedirectAttributes para limpiar atributos flash.
   */
  protected void clearErrorAndSuccessAttributes(RedirectAttributes red) {
    red.addFlashAttribute("failure", null);
    red.addFlashAttribute("success", null);
    red.addFlashAttribute("warning", null);
  }
}
