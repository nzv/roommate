package com.isil.roommate.controller;

import com.isil.roommate.entity.Booking;
import com.isil.roommate.entity.Booking.BookingBuilder;
import com.isil.roommate.entity.Guest;
import com.isil.roommate.exception.BookingOperationException;
import com.isil.roommate.exception.GuestOperationException;
import com.isil.roommate.service.BookingService;
import com.isil.roommate.service.GuestService;
import com.isil.roommate.service.RoomService;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador para gestionar operaciones relacionadas con reservas en el sistema.
 *
 * @see org.springframework.beans.factory.annotation.Autowired
 * @see org.springframework.security.access.prepost.PreAuthorize
 * @see org.springframework.stereotype.Controller
 * @see org.springframework.web.bind.annotation.GetMapping
 * @see org.springframework.web.bind.annotation.ModelAttribute
 * @see org.springframework.web.bind.annotation.PathVariable
 * @see org.springframework.web.bind.annotation.PostMapping
 * @see org.springframework.web.bind.annotation.RequestMapping
 * @see org.springframework.web.bind.annotation.RequestParam
 * @see org.springframework.web.servlet.mvc.support.RedirectAttributes
 */
@Controller
@RequestMapping("/reservas")
public class BookingController extends BaseController {

  @Autowired private RoomService roomService;
  @Autowired private GuestService guestService;
  @Autowired private BookingService bookingService;

  /**
   * Agrega las reservas como atributos flash.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   */
  private void addBookingsToFlashAttributes(RedirectAttributes red) {
    red.addFlashAttribute("bookings", bookingService.findAll());
  }

  /**
   * Mapeo para mostrar la lista de reservas. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping(value = {"", "/", "/listar"})
  @PreAuthorize("hasAuthority('RESERVA_LISTAR')")
  public String list(RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    addBookingsToFlashAttributes(red);
    return redirectToView("bookings/list", red);
  }

  /**
   * Mapeo para crear una nueva reserva. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @param resolveDni DNI opcional para resolver información del huésped.
   * @return Ruta de redirección.
   */
  @GetMapping("/crear")
  @PreAuthorize("hasAuthority('RESERVA_CREAR')")
  public String create(
      RedirectAttributes red,
      @RequestParam(name = "resolveDni", required = false) String resolveDni) {
    clearErrorAndSuccessAttributes(red);
    BookingBuilder booking = Booking.builder();
    try {
      if (resolveDni != null && !resolveDni.trim().isEmpty()) {
        if (guestService.existsByDni(resolveDni)) {
          Guest guest = guestService.findByDni(resolveDni);
          booking.guest(guest);
        } else {
          red.addFlashAttribute(
              "warning",
              "El cliente con el DNI '" + resolveDni + "' no existe en la base de datos.");
        }
      }
      red.addFlashAttribute("rooms", roomService.findAvailableRooms());
      red.addFlashAttribute("booking", booking.build());
      red.addFlashAttribute("nowDate", LocalDateTime.now().withNano(0));
    } catch (GuestOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      red.addFlashAttribute("booking", booking);
    }
    return redirectToView("bookings/create", red);
  }

  /**
   * Mapeo para procesar la creación de una reserva. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @param booking Objeto Booking que representa la reserva.
   * @return Ruta de redirección.
   */
  @PostMapping("/crear")
  @PreAuthorize("hasAuthority('RESERVA_CREAR')")
  public String create(RedirectAttributes red, @ModelAttribute Booking booking) {
    clearErrorAndSuccessAttributes(red);
    try {
      bookingService.save(booking);
      red.addFlashAttribute(
          "warning",
          "La reserva se cancelará automáticamente si no se efectúa el pago en las próximas 24"
              + " horas. Por favor, comuníquese con el cliente '"
              + booking.getGuest().getFullName()
              + "' mediante su número de teléfono +51 "
              + booking.getGuest().getPhoneNumber());
      red.addFlashAttribute("success", true);
    } catch (BookingOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      red.addFlashAttribute("booking", booking);
      red.addFlashAttribute("rooms", roomService.findAvailableRooms());
      return redirectToView("bookings/create", red);
    }
    addBookingsToFlashAttributes(red);
    return redirectToView("bookings/list", red);
  }

  /**
   * Mapeo para editar una reserva existente. Requiere autorización.
   *
   * @param id Identificador de la reserva a editar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping("/{id}/editar")
  @PreAuthorize("hasAuthority('RESERVA_EDITAR')")
  public String update(@PathVariable Long id, RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    try {
      Booking booking = bookingService.findById(id);
      red.addFlashAttribute("booking", booking);
      red.addFlashAttribute("nowDate", LocalDateTime.now().withNano(0));
    } catch (BookingOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      addBookingsToFlashAttributes(red);
      return redirectToView("bookings/list", red);
    }
    return redirectToView("bookings/edit", red);
  }

  /**
   * Mapeo para procesar la edición de una reserva. Requiere autorización.
   *
   * @param id Identificador de la reserva a editar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @param booking Objeto Booking que representa la reserva editada.
   * @return Ruta de redirección.
   */
  @PostMapping("/{id}/editar")
  @PreAuthorize("hasAuthority('RESERVA_EDITAR')")
  public String update(
      @PathVariable Long id, RedirectAttributes red, @ModelAttribute Booking booking) {
    clearErrorAndSuccessAttributes(red);
    try {
      booking.setId(id);
      bookingService.save(booking);
      red.addFlashAttribute("success", true);
    } catch (BookingOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      red.addFlashAttribute("booking", booking);
      return redirectToView("bookings/edit", red);
    }
    addBookingsToFlashAttributes(red);
    return redirectToView("bookings/list", red);
  }

  /**
   * Mapeo para eliminar una reserva. Requiere autorización.
   *
   * @param id Identificador de la reserva a eliminar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @PostMapping("/{id}/eliminar")
  @PreAuthorize("hasAuthority('RESERVA_ELIMINAR')")
  public String delete(@PathVariable Long id, RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    try {
      bookingService.deleteById(id);
      red.addFlashAttribute("success", true);
    } catch (BookingOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
    }
    return redirectToView("bookings/list", red);
  }
}
