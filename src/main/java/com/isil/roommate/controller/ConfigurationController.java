package com.isil.roommate.controller;

import com.isil.roommate.entity.ApplicationConfiguration;
import com.isil.roommate.entity.form.ApplicationConfigurationForm;
import com.isil.roommate.exception.ApplicationConfigurationException;
import com.isil.roommate.service.ApplicationConfigurationService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador para gestionar operaciones relacionadas con la configuración global de la aplicación.
 *
 * @see org.springframework.beans.factory.annotation.Autowired
 * @see org.springframework.security.access.prepost.PreAuthorize
 * @see org.springframework.stereotype.Controller
 * @see org.springframework.web.bind.annotation.GetMapping
 * @see org.springframework.web.bind.annotation.ModelAttribute
 * @see org.springframework.web.bind.annotation.PostMapping
 * @see org.springframework.web.bind.annotation.RequestMapping
 * @see org.springframework.web.servlet.mvc.support.RedirectAttributes
 */
@Controller
@RequestMapping("/configuracion")
public class ConfigurationController extends BaseController {

  private final ApplicationConfigurationService applicationConfigurationService;

  /**
   * Constructor que inyecta la dependencia de ApplicationConfigurationService.
   *
   * @param applicationConfigurationService Servicio para la configuración de la aplicación.
   */
  @Autowired
  public ConfigurationController(ApplicationConfigurationService applicationConfigurationService) {
    this.applicationConfigurationService = applicationConfigurationService;
  }

  /**
   * Agrega las configuraciones a los atributos flash para mostrar la lista. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  private void addConfigurationsToFlashAttributes(RedirectAttributes red) {
    List<ApplicationConfiguration> configs = applicationConfigurationService.findAll();
    ApplicationConfigurationForm form =
        ApplicationConfigurationForm.builder().applicationConfigurations(configs).build();
    red.addFlashAttribute("applicationConfigurationForm", form);
  }

  /**
   * Mapeo para mostrar la lista de configuraciones globales. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping(value = {"", "/"})
  @PreAuthorize("hasAuthority('CONFIGURACION_GLOBAL_LISTAR')")
  public String list(RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    addConfigurationsToFlashAttributes(red);
    return redirectToView("config/global", red);
  }

  /**
   * Mapeo para procesar la actualización de configuraciones globales. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @param form Objeto ApplicationConfigurationForm que representa las configuraciones a
   *     actualizar.
   * @return Ruta de redirección.
   */
  @PostMapping
  @PreAuthorize("hasAuthority('CONFIGURACION_GLOBAL_EDITAR')")
  public String update(RedirectAttributes red, @ModelAttribute ApplicationConfigurationForm form) {
    clearErrorAndSuccessAttributes(red);
    try {
      applicationConfigurationService.saveAll(form.getApplicationConfigurations());
      red.addFlashAttribute("success", true);
    } catch (ApplicationConfigurationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      addConfigurationsToFlashAttributes(red);
      return redirectToView("config/global", red);
    }
    addConfigurationsToFlashAttributes(red);
    return redirectToView("config/global", red);
  }
}
