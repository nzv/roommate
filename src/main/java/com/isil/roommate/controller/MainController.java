package com.isil.roommate.controller;

import com.isil.roommate.service.BookingService;
import com.isil.roommate.service.RoleService;
import com.isil.roommate.service.RoomService;
import com.isil.roommate.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controlador principal que gestiona las operaciones de la aplicación.
 *
 * @see org.springframework.beans.factory.annotation.Autowired
 * @see org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper
 * @see org.springframework.stereotype.Controller
 * @see org.springframework.ui.Model
 * @see org.springframework.web.bind.annotation.GetMapping
 * @see org.springframework.web.bind.annotation.RequestMapping
 */
@Controller
@RequestMapping
public class MainController {

  @Autowired private RoomService roomService;
  @Autowired private UserService userService;
  @Autowired private RoleService roleService;
  @Autowired private BookingService bookingService;

  /**
   * Mapeo para la página principal. Se adapta según la vista solicitada.
   *
   * @param aware SecurityContextHolderAwareRequestWrapper para manejar la autenticación.
   * @param model Modelo para agregar atributos a la vista.
   * @return Ruta de la página principal.
   */
  @GetMapping("/")
  public String index(SecurityContextHolderAwareRequestWrapper aware, Model model) {
    Object viewObject = model.asMap().get("view");
    if (viewObject != null && viewObject.toString().equals("home")) {
      model.addAttribute("roomService", roomService);
      model.addAttribute("roleService", roleService);
      model.addAttribute("userService", userService);
      model.addAttribute("bookingService", bookingService);
    }
    model.addAttribute("view", model.asMap().getOrDefault("view", "home"));
    return "index";
  }

  /**
   * Mapeo para la página de inicio de sesión.
   *
   * @return Ruta de la página de inicio de sesión.
   */
  @GetMapping("/login")
  public String login() {
    return "login";
  }
}
