package com.isil.roommate.controller;

import com.isil.roommate.entity.Role;
import com.isil.roommate.exception.RoleOperationException;
import com.isil.roommate.repository.AuthorityRepository;
import com.isil.roommate.service.RoleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador para gestionar operaciones relacionadas con roles en el sistema.
 *
 * @see org.springframework.beans.factory.annotation.Autowired
 * @see org.springframework.security.access.prepost.PreAuthorize
 * @see org.springframework.stereotype.Controller
 * @see org.springframework.web.bind.annotation.GetMapping
 * @see org.springframework.web.bind.annotation.ModelAttribute
 * @see org.springframework.web.bind.annotation.PostMapping
 * @see org.springframework.web.bind.annotation.RequestMapping
 * @see org.springframework.web.bind.annotation.RequestParam
 * @see org.springframework.web.servlet.mvc.support.RedirectAttributes
 */
@Controller
@RequestMapping("/roles")
public class RoleController extends BaseController {

  @Autowired private RoleService roleService;
  @Autowired private AuthorityRepository authorityRepository;

  /**
   * Agrega los roles a los atributos flash para mostrar la lista. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  private void addRolesToFlashAttributes(RedirectAttributes red) {
    List<Role> roles = roleService.findDistinctRoles();
    red.addFlashAttribute("roles", roles);
  }

  /**
   * Mapeo para mostrar la lista de roles. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping(value = {"", "/", "/listar"})
  @PreAuthorize("hasAuthority('ROL_LISTAR')")
  public String list(RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    addRolesToFlashAttributes(red);
    return redirectToView("roles/list", red);
  }

  /**
   * Mapeo para crear un nuevo rol. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping("/crear")
  @PreAuthorize("hasAuthority('ROL_CREAR')")
  public String create(RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    red.addFlashAttribute("authorities", authorityRepository.findAll());
    red.addFlashAttribute("role", Role.builder().build());
    return redirectToView("roles/create", red);
  }

  /**
   * Mapeo para procesar la creación de un nuevo rol. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @param authorityIds Lista de IDs de autoridades asociadas al rol.
   * @param role Objeto Role que representa el nuevo rol.
   * @return Ruta de redirección.
   */
  @PostMapping("/crear")
  @PreAuthorize("hasAuthority('ROL_CREAR')")
  public String create(
      RedirectAttributes red,
      @RequestParam("authorityIds") List<Long> authorityIds,
      @ModelAttribute Role role) {
    clearErrorAndSuccessAttributes(red);
    try {
      role.setAuthorities(authorityRepository.findByIdIn(authorityIds));
      roleService.save(role);
      red.addFlashAttribute("success", true);
    } catch (RoleOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      red.addFlashAttribute("role", role);
      red.addFlashAttribute("authorities", authorityRepository.findAll());
      return redirectToView("roles/create", red);
    }
    addRolesToFlashAttributes(red);
    return redirectToView("roles/list", red);
  }

  /**
   * Mapeo para editar un rol existente. Requiere autorización.
   *
   * @param id Identificador del rol a editar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping("/{id}/editar")
  @PreAuthorize("hasAuthority('ROL_EDITAR')")
  public String update(@PathVariable Long id, RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    try {
      Role role = roleService.findById(id);
      red.addFlashAttribute("role", role);
      red.addFlashAttribute("authorities", authorityRepository.findAll());
    } catch (RoleOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      addRolesToFlashAttributes(red);
      return redirectToView("roles/list", red);
    }
    return redirectToView("roles/edit", red);
  }

  /**
   * Mapeo para procesar la edición de un rol existente. Requiere autorización.
   *
   * @param id Identificador del rol a editar.
   * @param authorityIds Lista de IDs de autoridades asociadas al rol.
   * @param red RedirectAttributes para agregar atributos flash.
   * @param role Objeto Role que representa el rol editado.
   * @return Ruta de redirección.
   */
  @PostMapping("/{id}/editar")
  @PreAuthorize("hasAuthority('ROL_EDITAR')")
  public String update(
      @PathVariable Long id,
      @RequestParam("authorityIds") List<Long> authorityIds,
      RedirectAttributes red,
      @ModelAttribute Role role) {
    clearErrorAndSuccessAttributes(red);
    try {
      role.setId(id);
      role.setAuthorities(authorityRepository.findByIdIn(authorityIds));
      roleService.save(role);
      red.addFlashAttribute("success", true);
    } catch (RoleOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      red.addFlashAttribute("role", role);
      return redirectToView("roles/edit", red);
    }
    addRolesToFlashAttributes(red);
    return redirectToView("roles/list", red);
  }

  /**
   * Mapeo para eliminar un rol. Requiere autorización.
   *
   * @param id Identificador del rol a eliminar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @PostMapping("/{id}/eliminar")
  @PreAuthorize("hasAuthority('ROL_ELIMINAR')")
  public String delete(@PathVariable Long id, RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    try {
      roleService.deleteById(id);
      red.addFlashAttribute("success", true);
    } catch (RoleOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
    }
    addRolesToFlashAttributes(red);
    return redirectToView("roles/list", red);
  }
}
