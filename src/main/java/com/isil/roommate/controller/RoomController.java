package com.isil.roommate.controller;

import com.isil.roommate.entity.Room;
import com.isil.roommate.exception.RoomOperationException;
import com.isil.roommate.service.BookingService;
import com.isil.roommate.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador para gestionar operaciones relacionadas con las habitaciones.
 *
 * @see org.springframework.beans.factory.annotation.Autowired
 * @see org.springframework.security.access.prepost.PreAuthorize
 * @see org.springframework.stereotype.Controller
 * @see org.springframework.web.bind.annotation.GetMapping
 * @see org.springframework.web.bind.annotation.ModelAttribute
 * @see org.springframework.web.bind.annotation.PostMapping
 * @see org.springframework.web.bind.annotation.RequestMapping
 * @see org.springframework.web.servlet.mvc.support.RedirectAttributes
 */
@Controller
@RequestMapping("/habitaciones")
public class RoomController extends BaseController {

  @Autowired private RoomService roomService;
  @Autowired private BookingService bookingService;

  /**
   * Agrega las habitaciones a los atributos flash para mostrar la lista. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  private void addRoomsToFlashAttributes(RedirectAttributes red) {
    red.addFlashAttribute("rooms", roomService.findAll());
  }

  /**
   * Mapeo para mostrar la lista de habitaciones. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping(value = {"", "/", "/listar"})
  @PreAuthorize("hasAuthority('HABITACION_LISTAR')")
  public String list(RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    addRoomsToFlashAttributes(red);
    return redirectToView("rooms/list", red);
  }

  /**
   * Mapeo para crear una nueva habitación. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping("/crear")
  @PreAuthorize("hasAuthority('HABITACION_CREAR')")
  public String create(RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    red.addFlashAttribute("room", Room.builder().build());
    return redirectToView("rooms/create", red);
  }

  /**
   * Mapeo para procesar la creación de una nueva habitación. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @param room Objeto Room que representa la nueva habitación.
   * @return Ruta de redirección.
   */
  @PostMapping("/crear")
  @PreAuthorize("hasAuthority('HABITACION_CREAR')")
  public String create(RedirectAttributes red, @ModelAttribute Room room) {
    clearErrorAndSuccessAttributes(red);
    try {
      roomService.save(room);
      red.addFlashAttribute("success", true);
    } catch (RoomOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      red.addFlashAttribute("room", room);
      return redirectToView("rooms/create", red);
    }
    addRoomsToFlashAttributes(red);
    return redirectToView("rooms/list", red);
  }

  /**
   * Mapeo para editar una habitación existente. Requiere autorización.
   *
   * @param id Identificador de la habitación a editar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping("/{id}/editar")
  @PreAuthorize("hasAuthority('HABITACION_EDITAR')")
  public String update(@PathVariable Long id, RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    try {
      Room room = roomService.findById(id);
      red.addFlashAttribute("room", room);
      red.addFlashAttribute("isRoomBooked", bookingService.isRoomBooked(id));
    } catch (RoomOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      addRoomsToFlashAttributes(red);
      return redirectToView("rooms/list", red);
    }
    return redirectToView("rooms/edit", red);
  }

  /**
   * Mapeo para procesar la edición de una habitación existente. Requiere autorización.
   *
   * @param id Identificador de la habitación a editar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @param room Objeto Room que representa la habitación editada.
   * @return Ruta de redirección.
   */
  @PostMapping("/{id}/editar")
  @PreAuthorize("hasAuthority('HABITACION_EDITAR')")
  public String update(@PathVariable Long id, RedirectAttributes red, @ModelAttribute Room room) {
    clearErrorAndSuccessAttributes(red);
    try {
      room.setId(id);
      roomService.save(room);
      red.addFlashAttribute("success", true);
    } catch (RoomOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
    }
    addRoomsToFlashAttributes(red);
    return redirectToView("rooms/list", red);
  }

  /**
   * Mapeo para eliminar una habitación. Requiere autorización.
   *
   * @param aware SecurityContextHolderAwareRequestWrapper para manejar la autenticación.
   * @param id Identificador de la habitación a eliminar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @PostMapping("/{id}/eliminar")
  @PreAuthorize("hasAuthority('HABITACION_ELIMINAR')")
  public String delete(
      SecurityContextHolderAwareRequestWrapper aware,
      @PathVariable Long id,
      RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    try {
      roomService.deleteById(id);
      red.addFlashAttribute("success", true);
    } catch (RoomOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
    }
    addRoomsToFlashAttributes(red);
    return redirectToView("rooms/list", red);
  }
}
