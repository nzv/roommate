package com.isil.roommate.controller;

import com.isil.roommate.entity.User;
import com.isil.roommate.exception.UserOperationException;
import com.isil.roommate.service.RoleService;
import com.isil.roommate.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador para gestionar operaciones relacionadas con los usuarios.
 *
 * @see org.springframework.beans.factory.annotation.Autowired
 * @see org.springframework.security.access.prepost.PreAuthorize
 * @see org.springframework.stereotype.Controller
 * @see org.springframework.web.bind.annotation.GetMapping
 * @see org.springframework.web.bind.annotation.ModelAttribute
 * @see org.springframework.web.bind.annotation.PostMapping
 * @see org.springframework.web.bind.annotation.RequestMapping
 * @see org.springframework.web.servlet.mvc.support.RedirectAttributes
 */
@Controller
@RequestMapping("/usuarios")
public class UserController extends BaseController {

  private final UserService userService;
  private final RoleService roleService;

  @Autowired
  public UserController(UserService userService, RoleService roleService) {
    this.userService = userService;
    this.roleService = roleService;
  }

  /**
   * Agrega los usuarios a los atributos flash para mostrar la lista. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  private void addUsersToFlashAttributes(RedirectAttributes red) {
    List<User> users = userService.findAll();
    red.addFlashAttribute("users", users);
  }

  /**
   * Mapeo para mostrar la lista de usuarios. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping(value = {"", "/", "/listar"})
  @PreAuthorize("hasAuthority('USUARIO_LISTAR')")
  public String list(RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    addUsersToFlashAttributes(red);
    return redirectToView("users/list", red);
  }

  /**
   * Mapeo para crear un nuevo usuario. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping("/crear")
  @PreAuthorize("hasAuthority('USUARIO_CREAR')")
  public String create(RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    red.addFlashAttribute("roles", roleService.findDistinctRoles());
    red.addFlashAttribute("user", User.builder().build());
    return redirectToView("users/create", red);
  }

  /**
   * Mapeo para procesar la creación de un nuevo usuario. Requiere autorización.
   *
   * @param red RedirectAttributes para agregar atributos flash.
   * @param user Objeto User que representa el nuevo usuario.
   * @return Ruta de redirección.
   */
  @PostMapping("/crear")
  @PreAuthorize("hasAuthority('USUARIO_CREAR')")
  public String create(RedirectAttributes red, @ModelAttribute User user) {
    clearErrorAndSuccessAttributes(red);
    try {
      userService.save(user);
      red.addFlashAttribute("success", true);
    } catch (UserOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      red.addFlashAttribute("user", user);
      red.addFlashAttribute("roles", roleService.findDistinctRoles());
      return redirectToView("users/create", red);
    }
    addUsersToFlashAttributes(red);
    return redirectToView("users/list", red);
  }

  /**
   * Mapeo para editar un usuario existente. Requiere autorización.
   *
   * @param id Identificador del usuario a editar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @GetMapping("/{id}/editar")
  @PreAuthorize("hasAuthority('USUARIO_EDITAR')")
  public String update(@PathVariable Long id, RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    try {
      User user = userService.findById(id);
      red.addFlashAttribute("user", user);
      red.addFlashAttribute("roles", roleService.findDistinctRoles());
    } catch (UserOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      addUsersToFlashAttributes(red);
      return redirectToView("users/list", red);
    }
    return redirectToView("users/edit", red);
  }

  /**
   * Mapeo para procesar la edición de un usuario existente. Requiere autorización.
   *
   * @param id Identificador del usuario a editar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @param user Objeto User que representa el usuario editado.
   * @return Ruta de redirección.
   */
  @PostMapping("/{id}/editar")
  @PreAuthorize("hasAuthority('USUARIO_EDITAR')")
  public String update(@PathVariable Long id, RedirectAttributes red, @ModelAttribute User user) {
    clearErrorAndSuccessAttributes(red);
    try {
      user.setId(id);
      userService.save(user);
      red.addFlashAttribute("success", true);
    } catch (UserOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
      red.addFlashAttribute("user", user);
      red.addFlashAttribute("roles", roleService.findDistinctRoles());
      return redirectToView("users/edit", red);
    }
    addUsersToFlashAttributes(red);
    return redirectToView("users/list", red);
  }

  /**
   * Mapeo para eliminar un usuario. Requiere autorización.
   *
   * @param aware SecurityContextHolderAwareRequestWrapper para manejar la autenticación.
   * @param id Identificador del usuario a eliminar.
   * @param red RedirectAttributes para agregar atributos flash.
   * @return Ruta de redirección.
   */
  @PostMapping("/{id}/eliminar")
  @PreAuthorize("hasAuthority('USUARIO_ELIMINAR')")
  public String delete(
      SecurityContextHolderAwareRequestWrapper aware,
      @PathVariable Long id,
      RedirectAttributes red) {
    clearErrorAndSuccessAttributes(red);
    try {
      userService.deleteById(aware, id);
      red.addFlashAttribute("success", true);
    } catch (UserOperationException ex) {
      red.addFlashAttribute("failure", ex.getMessage());
    }
    addUsersToFlashAttributes(red);
    return redirectToView("users/list", red);
  }
}
