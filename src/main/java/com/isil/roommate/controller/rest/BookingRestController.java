package com.isil.roommate.controller.rest;

import com.isil.roommate.entity.Booking;
import com.isil.roommate.service.BookingService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador REST que sirve como API para fullcalendar.js, gestionando operaciones relacionadas
 * con reservas.
 *
 * @see org.springframework.beans.factory.annotation.Autowired
 * @see org.springframework.security.access.prepost.PreAuthorize
 * @see org.springframework.web.bind.annotation.GetMapping
 * @see org.springframework.web.bind.annotation.RequestMapping
 * @see org.springframework.web.bind.annotation.RestController
 */
@RestController
@RequestMapping("/rest/reservas")
public class BookingRestController {

  @Autowired private BookingService bookingService;

  /**
   * Mapeo para obtener la lista de reservas. Requiere autorización.
   *
   * @return Lista de objetos Booking que representan las reservas.
   */
  @GetMapping(value = {"", "/", "/listar"})
  @PreAuthorize("hasAuthority('RESERVA_LISTAR')")
  public List<Booking> list() {
    return bookingService.findAll();
  }
}
