package com.isil.roommate.entity;

import com.isil.roommate.hibernate.BaseEntity;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Clase que representa la configuración de la aplicación.
 *
 * <p>Esta entidad extiende de {@link com.isil.roommate.hibernate.BaseEntity} para heredar
 * funcionalidades comunes de persistencia.
 *
 * @see javax.persistence.Entity
 * @see lombok.AllArgsConstructor
 * @see lombok.Getter
 * @see lombok.NoArgsConstructor
 * @see lombok.Setter
 * @see lombok.experimental.SuperBuilder
 */
@Setter
@Getter
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationConfiguration extends BaseEntity {

  /** Clave de configuración. */
  private String configKey;

  /** Valor de configuración. */
  private String value;

  /** Descripción de la configuración. */
  private String description;
}
