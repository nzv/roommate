package com.isil.roommate.entity;

import com.isil.roommate.hibernate.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.SQLDelete;

/**
 * Clase que representa una autoridad en la aplicación.
 *
 * <p>Esta entidad extiende de {@link com.isil.roommate.hibernate.BaseEntity} para heredar
 * funcionalidades comunes de persistencia.
 *
 * @see javax.persistence.Entity
 * @see javax.persistence.Column
 * @see lombok.AllArgsConstructor
 * @see lombok.Getter
 * @see lombok.NoArgsConstructor
 * @see lombok.Setter
 * @see lombok.experimental.SuperBuilder
 * @see org.hibernate.annotations.SQLDelete
 */
@Setter
@Getter
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE authority SET enabled = false WHERE id=?")
public class Authority extends BaseEntity {

  /** Nombre único de la autoridad. */
  @Column(unique = true)
  private String name;

}
