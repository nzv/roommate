package com.isil.roommate.entity;

import com.isil.roommate.hibernate.BaseEntity;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.annotation.Transient;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Clase que representa una reserva en la aplicación.
 *
 * <p>Esta entidad extiende de {@link com.isil.roommate.hibernate.BaseEntity} para heredar
 * funcionalidades comunes de persistencia.
 *
 * @see javax.persistence.Entity
 * @see javax.persistence.EnumType
 * @see javax.persistence.PreUpdate
 * @see javax.persistence.Enumerated
 * @see javax.persistence.JoinColumn
 * @see javax.persistence.ManyToOne
 * @see javax.persistence.PrePersist
 * @see lombok.Getter
 * @see lombok.Setter
 * @see lombok.experimental.SuperBuilder
 * @see org.hibernate.annotations.SQLDelete
 * @see org.springframework.data.annotation.Transient
 * @see org.springframework.format.annotation.DateTimeFormat
 */
@Getter
@Setter
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE booking SET enabled = false WHERE id=?")
public class Booking extends BaseEntity {

  /** Título de la reserva. */
  @Transient private String title;

  /** Habitación asociada a la reserva. */
  @ManyToOne
  @JoinColumn(name = "room_id", nullable = false)
  private Room room;

  /** Fecha y hora de inicio de la reserva. */
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private LocalDateTime start;

  /** Fecha y hora de fin de la reserva. */
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private LocalDateTime end;

  /** Estado de la reserva. */
  @Enumerated(EnumType.STRING)
  private BookingStatus status;

  /** Color asociado al estado de la reserva. */
  private String color;

  /** Huésped asociado a la reserva. */
  @ManyToOne
  @JoinColumn(name = "guest_id", nullable = false)
  private Guest guest;

  /** Precio total de la reserva. */
  private BigDecimal price;

  /**
   * Método ejecutado antes de la persistencia o actualización de la reserva. Se encarga de
   * configurar el título, el color y el precio de la reserva según su estado y duración.
   */
  @PrePersist
  @PreUpdate
  public void prePersistOrUpdate() {
    this.title = "Reservación " + room.getRoomNumber();
    this.start = this.start.withNano(0);
    this.end = this.end.withNano(0);
    switch (this.status) {
      case PENDING:
        this.color = "#F4CE14"; // Amarillo
        break;
      case CONFIRMED:
        this.color = "#00FF00"; // Verde
        break;
      case CANCELLED:
        this.color = "#FF0000"; // Rojo
        break;
      case CHECKED_IN:
        this.color = "#0000FF"; // Azul
        break;
      case CHECKED_OUT:
        this.color = "#800080"; // Morado
        break;
      default:
        this.color = "#000000"; // Color predeterminado (negro) para estados desconocidos
        break;
    }
    Duration duration = Duration.between(start, end);
    long nights = duration.toDays();
    BigDecimal pricePerNight = room.getPricePerNight();
    this.price = pricePerNight.multiply(BigDecimal.valueOf(nights));
  }

}
