package com.isil.roommate.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enumeración que representa los distintos estados de una reserva en la aplicación.
 *
 * @see lombok.Getter
 * @see lombok.RequiredArgsConstructor
 */
@RequiredArgsConstructor
@Getter
public enum BookingStatus {
  // Pendiente
  PENDING("Pendiente"),
  // Confirmado
  CONFIRMED("Confirmado"),
  // Cancelado
  CANCELLED("Cancelado"),
  // Registrado
  CHECKED_IN("Registrado"),
  // Registrado y Pagado
  CHECKED_OUT("Registrado y Pagado");

  /**
   * Obtiene el valor de visualización asociado al estado.
   *
   * @return Valor de visualización del estado.
   */
  private final String displayValue;
}
