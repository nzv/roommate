package com.isil.roommate.entity;

import com.isil.roommate.hibernate.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.SQLDelete;

/**
 * Clase que representa a un huésped en la aplicación.
 *
 * <p>Un huésped tiene atributos como nombre, DNI, apellidos, número de teléfono y correo
 * electrónico.
 *
 * @see com.isil.roommate.hibernate.BaseEntity
 * @see javax.persistence.Entity
 * @see javax.persistence.Table
 * @see javax.persistence.UniqueConstraint
 * @see lombok.Getter
 * @see lombok.Setter
 * @see lombok.experimental.SuperBuilder
 * @see org.hibernate.annotations.SQLDelete
 */
@Getter
@Setter
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"dni", "email", "phoneNumber"})})
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE guest SET enabled = false WHERE id=?")
public class Guest extends BaseEntity {
  /** Nombre del huésped. */
  private String name;

  /** Número de DNI del huésped. */
  private String dni;

  /** Apellido materno del huésped. */
  private String motherLastName;

  /** Apellido paterno del huésped. */
  private String fatherLastName;

  /** Número de teléfono del huésped. */
  private String phoneNumber;

  /** Correo electrónico del huésped. */
  private String email;

  /**
   * Obtiene el nombre completo del huésped.
   *
   * @return Nombre completo del huésped.
   */
  public String getFullName() {
    return String.format("%s %s %s", name, fatherLastName, motherLastName);
  }

}
