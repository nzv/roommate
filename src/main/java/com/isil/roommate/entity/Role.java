package com.isil.roommate.entity;

import com.isil.roommate.hibernate.BaseEntity;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.SQLDelete;

/**
 * Clase que representa un rol en la aplicación.
 *
 * <p>Un rol tiene un nombre único y puede estar asociado a usuarios y poseer diversas autoridades.
 *
 * @see com.isil.roommate.hibernate.BaseEntity
 * @see javax.persistence.Entity
 * @see javax.persistence.Column
 * @see javax.persistence.FetchType
 * @see javax.persistence.JoinColumn
 * @see javax.persistence.JoinTable
 * @see javax.persistence.ManyToMany
 * @see javax.validation.constraints.NotNull
 * @see lombok.Getter
 * @see lombok.Setter
 * @see lombok.experimental.SuperBuilder
 * @see org.hibernate.annotations.SQLDelete
 */
@Setter
@Getter
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE role SET enabled = false WHERE id=?")
public class Role extends BaseEntity {

  /** Nombre único del rol. */
  @NotNull
  @Column(unique = true)
  private String name;

  /** Usuario asociado al rol. */
  private User user;

  /** Colección de autoridades asociadas al rol. */
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "role_authorities",
      joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))
  private Collection<Authority> authorities;

  /**
   * Verifica si el rol tiene una autoridad específica.
   *
   * @param authorityId Identificador de la autoridad a verificar.
   * @return true si el rol tiene la autoridad, false de lo contrario.
   */
  public boolean hasAuthority(Long authorityId) {
    return authorities.stream().anyMatch(authority -> authority.getId().equals(authorityId));
  }

}
