package com.isil.roommate.entity;

import com.isil.roommate.hibernate.BaseEntity;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.SQLDelete;

/**
 * Clase que representa una habitación en la aplicación.
 *
 * <p>Una habitación tiene un número identificador único, un tipo, un precio por noche, capacidad
 * máxima, número de piso, fecha de la última fecha de mantenimiento y un estado.
 *
 * @see com.isil.roommate.hibernate.BaseEntity
 * @see javax.persistence.Entity
 * @see javax.persistence.EnumType
 * @see javax.persistence.Enumerated
 * @see lombok.AllArgsConstructor
 * @see lombok.Builder
 * @see lombok.Getter
 * @see lombok.NoArgsConstructor
 * @see lombok.Setter
 * @see lombok.experimental.SuperBuilder
 * @see org.hibernate.annotations.SQLDelete
 * @see java.math.BigDecimal
 * @see java.time.LocalDate
 */
@Getter
@Setter
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE room SET enabled = false WHERE id=?")
public class Room extends BaseEntity {

  /** Número identificador único de la habitación. */
  private int roomNumber;

  /** Tipo de habitación. */
  @Enumerated(EnumType.STRING)
  private RoomType type;

  /** Precio por noche de la habitación. */
  private BigDecimal pricePerNight;

  /** Capacidad máxima de la habitación. */
  private int capacity;

  /** Número de piso de la habitación. */
  private int floor;

  /** Fecha de la última fecha de mantenimiento de la habitación. */
  @Builder.Default private LocalDate lastMaintenanceDate = LocalDate.now();

  /** Estado actual de la habitación. */
  @Enumerated(EnumType.STRING)
  private RoomStatus status;

}
