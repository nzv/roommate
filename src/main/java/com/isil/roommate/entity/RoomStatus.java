package com.isil.roommate.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enumeración que representa el estado de una habitación en la aplicación.
 *
 * <p>Los posibles estados de una habitación son Limpio, Por limpiar y En mantenimiento.
 *
 * @see lombok.Getter
 * @see lombok.RequiredArgsConstructor
 */
@RequiredArgsConstructor
@Getter
public enum RoomStatus {
  /** Estado de la habitación cuando está limpia. */
  CLEAN("Limpio"),
  /** Estado de la habitación cuando está por limpiar. */
  DIRTY("Por limpiar"),
  /** Estado de la habitación cuando está en mantenimiento. */
  UNDER_MAINTENANCE("En mantenimiento");

  /** Valor de visualización del estado. */
  private final String displayValue;
}
