package com.isil.roommate.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enumeración que representa el tipo de una habitación en la aplicación.
 *
 * <p>Los posibles tipos de una habitación son Individual y Doble.
 *
 * @see lombok.Getter
 * @see lombok.RequiredArgsConstructor
 */
@RequiredArgsConstructor
@Getter
public enum RoomType {
  /** Tipo de habitación individual. */
  SINGLE("Individual"),
  /** Tipo de habitación doble. */
  DOUBLE("Doble");

  /** Valor de visualización del tipo de habitación. */
  private final String displayValue;
}
