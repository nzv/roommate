package com.isil.roommate.entity;

import com.isil.roommate.hibernate.BaseEntity;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.annotation.Transient;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Clase que representa a un usuario en la aplicación.
 *
 * <p>Implementa la interfaz UserDetails de Spring Security para gestionar la autenticación y
 * autorización del usuario.
 *
 * @see com.isil.roommate.hibernate.BaseEntity
 * @see org.springframework.security.core.userdetails.UserDetails
 * @see lombok.Setter
 * @see lombok.Getter
 * @see lombok.experimental.SuperBuilder
 * @see org.hibernate.annotations.SQLDelete
 */
@Setter
@Getter
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@SQLDelete(sql = "UPDATE user SET enabled = false WHERE id=?")
public class User extends BaseEntity implements UserDetails {

  /** Nombre de usuario del usuario. Debe ser único. */
  @Column(unique = true)
  @Size(min = 8, max = 8)
  private String username;

  /** Campo transitorio para la contraseña. */
  @Transient private String password;

  /**
   * Número de intentos fallidos de inicio de sesión. Almacena la cantidad de intentos de inicio de
   * sesión fallidos para un usuario.
   */
  @Column(name = "failed_attempt")
  private int failedAttempt;

  /**
   * Hora de bloqueo después de intentos fallidos. Almacena la hora de bloqueo que se actualiza cada
   * vez que se excede el límite de intentos fallidos de inicio de sesión.
   */
  @Builder.Default
  @Column(name = "lock_time")
  private LocalDateTime lockTime = LocalDateTime.now();

  /** Rol del usuario. */
  @ManyToOne
  @JoinColumn(name = "role_id")
  private Role role;

  /**
   * Devuelve las autoridades del usuario, en este caso, el rol con el prefijo "ROLE_".
   *
   * @return Colección de GrantedAuthority que representa los roles del usuario.
   */
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return this.role.getAuthorities().stream()
        .map(authority -> new SimpleGrantedAuthority(authority.getName()))
        .collect(Collectors.toList());
  }

  /**
   * Devuelve si la cuenta del usuario no ha caducado.
   *
   * @return Siempre devuelve true.
   */
  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  /**
   * Devuelve si la cuenta del usuario no está bloqueada.
   *
   * @return Siempre devuelve true si el tiempo de bloqueo ha pasado.
   */
  @Override
  public boolean isAccountNonLocked() {
    return LocalDateTime.now().isAfter(lockTime);
  }

  /**
   * Devuelve si las credenciales del usuario (contraseña) no han caducado.
   *
   * @return Siempre devuelve true.
   */
  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  /**
   * Devuelve si el usuario está habilitado.
   *
   * @return true si el usuario está habilitado.
   */
  @Override
  public boolean isEnabled() {
    return this.enabled;
  }

}
