package com.isil.roommate.entity.form;

import com.isil.roommate.entity.ApplicationConfiguration;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Clase que representa el formulario de configuración de la aplicación.
 *
 * @see com.isil.roommate.entity.ApplicationConfiguration
 * @see java.util.List
 * @see lombok.Setter
 * @see lombok.Getter
 * @see lombok.Builder
 * @see lombok.AllArgsConstructor
 * @see lombok.NoArgsConstructor
 */
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationConfigurationForm {

  /** Lista de configuraciones de la aplicación. */
  private List<ApplicationConfiguration> applicationConfigurations;

}
