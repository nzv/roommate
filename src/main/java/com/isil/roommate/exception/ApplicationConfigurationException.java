package com.isil.roommate.exception;

import lombok.experimental.StandardException;

/**
 * Excepción personalizada para problemas relacionados con la configuración de la aplicación.
 *
 * @see lombok.experimental.StandardException
 */
@StandardException
public class ApplicationConfigurationException extends Exception {}
