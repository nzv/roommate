package com.isil.roommate.exception;

import lombok.experimental.StandardException;

/**
 * Excepción personalizada para problemas relacionados con operaciones de reserva.
 *
 * @see lombok.experimental.StandardException
 */
@StandardException
public class BookingOperationException extends Exception {}
