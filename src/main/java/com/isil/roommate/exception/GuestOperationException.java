package com.isil.roommate.exception;

import lombok.experimental.StandardException;

/**
 * Excepción personalizada para problemas relacionados con operaciones de huéspedes.
 *
 * @see lombok.experimental.StandardException
 */
@StandardException
public class GuestOperationException extends Exception {}
