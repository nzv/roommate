package com.isil.roommate.exception;

import lombok.experimental.StandardException;

/**
 * Excepción personalizada para problemas relacionados con operaciones de roles.
 *
 * @see lombok.experimental.StandardException
 */
@StandardException
public class RoleOperationException extends Exception {}
