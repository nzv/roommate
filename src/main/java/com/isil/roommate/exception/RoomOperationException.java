package com.isil.roommate.exception;

import lombok.experimental.StandardException;

/**
 * Excepción personalizada para problemas relacionados con operaciones de habitaciones.
 *
 * @see lombok.experimental.StandardException
 */
@StandardException
public class RoomOperationException extends Exception {}
