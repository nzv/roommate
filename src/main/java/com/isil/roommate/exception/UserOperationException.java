package com.isil.roommate.exception;

import lombok.experimental.StandardException;

/**
 * Excepción personalizada para problemas relacionados con operaciones de usuarios.
 *
 * @see lombok.experimental.StandardException
 */
@StandardException
public class UserOperationException extends Exception {}
