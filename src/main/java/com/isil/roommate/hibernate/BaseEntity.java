package com.isil.roommate.hibernate;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Clase base para todas las entidades en la aplicación, proporcionando campos de auditoría y manejo
 * de habilitación.
 */
@Getter
@Setter
@SuperBuilder
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@MappedSuperclass
@FilterDef(name = "enabledFilter", parameters = @ParamDef(name = "isEnabled", type = "boolean"))
@Filter(name = "enabledFilter", condition = "enabled = :isEnabled")
public class BaseEntity {

  /** Identificador único para la entidad */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  /** Indica si la entidad está habilitada o deshabilitada. */
  @Builder.Default protected boolean enabled = true;

  /** Creador de la entidad. No se puede modificar. */
  @CreatedBy
  @Column(nullable = false, updatable = false)
  protected String creator;

  /** Fecha y hora de creación de la entidad. No se puede modificar. */
  @CreatedDate
  @Column(nullable = false, updatable = false)
  protected LocalDateTime created;

  /** Modificador de la entidad. */
  @LastModifiedBy
  @Column(nullable = false)
  protected String modifier;

  /** Fecha y hora de la última modificación de la entidad */
  @LastModifiedDate
  @Column(nullable = false)
  protected LocalDateTime modified;

}
