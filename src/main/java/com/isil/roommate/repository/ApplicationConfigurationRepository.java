package com.isil.roommate.repository;

import com.isil.roommate.entity.ApplicationConfiguration;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/** Repositorio para la entidad {@link ApplicationConfiguration}. */
@Repository
public interface ApplicationConfigurationRepository
    extends CrudRepository<ApplicationConfiguration, Long> {

  /**
   * Obtiene una lista de todas las configuraciones de la aplicación.
   *
   * @return Lista de {@link ApplicationConfiguration}.
   */
  List<ApplicationConfiguration> findAll();

  /**
   * Obtiene una configuración de la aplicación por su identificador.
   *
   * @param id Identificador de la configuración.
   * @return {@link Optional} que puede contener la {@link ApplicationConfiguration} si se
   *     encuentra.
   */
  Optional<ApplicationConfiguration> findById(long id);

  /**
   * Busca una configuración de la aplicación por su clave de configuración.
   *
   * @param configKey Clave de configuración a buscar.
   * @return {@link ApplicationConfiguration} si se encuentra.
   */
  ApplicationConfiguration findByConfigKey(String configKey);
}
