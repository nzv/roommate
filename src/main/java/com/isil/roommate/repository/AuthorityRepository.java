package com.isil.roommate.repository;

import com.isil.roommate.entity.Authority;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/** Repositorio para la entidad {@link Authority}. */
@Repository
public interface AuthorityRepository extends CrudRepository<Authority, Long> {

  /**
   * Obtiene una lista de autoridades por sus identificadores.
   *
   * @param authorityIds Lista de identificadores de autoridad.
   * @return Lista de {@link Authority}.
   */
  List<Authority> findByIdIn(List<Long> authorityIds);

  /**
   * Busca una autoridad por su nombre.
   *
   * @param name Nombre de la autoridad a buscar.
   * @return {@link Authority} si se encuentra.
   */
  Authority findByName(String name);

  /**
   * Obtiene una lista de autoridades cuyos nombres comienzan con el prefijo dado.
   *
   * @param prefix Prefijo para buscar autoridades.
   * @return Lista de {@link Authority}.
   */
  List<Authority> findByNameStartsWith(String prefix);
}
