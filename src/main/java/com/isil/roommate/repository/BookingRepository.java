package com.isil.roommate.repository;

import com.isil.roommate.entity.Booking;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/** Repositorio para la entidad {@link Booking}. */
public interface BookingRepository extends CrudRepository<Booking, Long> {

  /**
   * Obtiene todas las reservas.
   *
   * @return Lista de {@link Booking}.
   */
  List<Booking> findAll();

  /**
   * Busca una reserva por su identificador.
   *
   * @param id Identificador de la reserva.
   * @return {@link Optional} que contiene la {@link Booking} si se encuentra.
   */
  Optional<Booking> findById(long id);

  /**
   * Verifica si una habitación está reservada.
   *
   * @param roomId Identificador de la habitación.
   * @return {@code true} si la habitación está reservada, {@code false} de lo contrario.
   */
  @Query("SELECT COUNT(b) > 0 FROM Booking b WHERE b.room.id = :roomId")
  boolean isRoomBooked(@Param("roomId") Long roomId);

  /**
   * Obtiene las reservas pendientes que fueron creadas antes de la marca de tiempo dada.
   *
   * @param timestamp Marca de tiempo.
   * @return Lista de {@link Booking} pendientes.
   */
  @Query("SELECT b FROM Booking b WHERE b.status = 'PENDING' AND b.created < :timestamp")
  List<Booking> findPendingBookingsOlderThan(@Param("timestamp") LocalDateTime timestamp);
}
