package com.isil.roommate.repository;

import com.isil.roommate.entity.Guest;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

/** Repositorio para la entidad {@link Guest}. */
public interface GuestRepository extends CrudRepository<Guest, Long> {

  /**
   * Obtiene una lista de todos los huéspedes.
   *
   * @return Lista de {@link Guest}.
   */
  List<Guest> findAll();

  /**
   * Busca un huésped por su identificador.
   *
   * @param id Identificador del huésped.
   * @return {@link Optional} que contiene el {@link Guest} si se encuentra.
   */
  Optional<Guest> findById(long id);

  /**
   * Busca un huésped por su número de documento de identidad (DNI).
   *
   * @param dni Número de documento de identidad (DNI) del huésped.
   * @return {@link Optional} que contiene el {@link Guest} si se encuentra.
   */
  Optional<Guest> findByDni(String dni);

  /**
   * Verifica si existe un huésped con el número de documento de identidad (DNI) dado.
   *
   * @param dni Número de documento de identidad (DNI) a verificar.
   * @return true si existe un huésped con el DNI dado, false de lo contrario.
   */
  boolean existsByDni(String dni);
}
