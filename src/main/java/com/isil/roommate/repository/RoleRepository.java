package com.isil.roommate.repository;

import com.isil.roommate.entity.Role;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/** Repositorio para la entidad {@link Role}. */
@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

  /**
   * Obtiene una lista de roles distintos con sus autoridades asociadas.
   *
   * @return Lista de {@link Role} con autoridades.
   */
  @Query("SELECT DISTINCT r FROM Role r LEFT JOIN FETCH r.authorities")
  List<Role> findDistinctRoles();

  /**
   * Busca el primer rol por su nombre.
   *
   * @param name Nombre del rol a buscar.
   * @return {@link Role} encontrado, o null si no hay coincidencias.
   */
  Role findFirstByName(String name);

  /**
   * Obtiene una lista de todos los roles.
   *
   * @return Lista de {@link Role}.
   */
  List<Role> findAll();

  /**
   * Verifica si existe un rol con el nombre dado.
   *
   * @param name Nombre del rol a verificar.
   * @return true si existe un rol con el nombre dado, false de lo contrario.
   */
  boolean existsByName(String name);
}
