package com.isil.roommate.repository;

import com.isil.roommate.entity.BookingStatus;
import com.isil.roommate.entity.Room;
import com.isil.roommate.entity.RoomStatus;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/** Repositorio para la entidad {@link Room}. */
@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {

  /**
   * Obtiene una lista de todas las habitaciones.
   *
   * @return Lista de {@link Room}.
   */
  List<Room> findAll();

  /**
   * Encuentra habitaciones disponibles que estén limpias y no reservadas.
   *
   * @param cleanStatus Estado de limpieza de la habitación.
   * @param bookingStatus Lista de estados de reserva.
   * @return Lista de habitaciones disponibles.
   */
  @Query(
      "SELECT r FROM Room r WHERE r.status = :cleanStatus AND r NOT IN (SELECT b.room FROM Booking"
          + " b WHERE b.status IN :bookingStatus)")
  List<Room> findAvailableRooms(
      @Param("cleanStatus") RoomStatus cleanStatus,
      @Param("bookingStatus") List<BookingStatus> bookingStatus);

  /**
   * Verifica si existe una habitación con el número dado.
   *
   * @param roomNumber Número de habitación a verificar.
   * @return true si existe una habitación con el número dado, false de lo contrario.
   */
  boolean existsByRoomNumber(int roomNumber);
}
