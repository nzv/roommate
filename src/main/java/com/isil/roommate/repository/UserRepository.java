package com.isil.roommate.repository;

import com.isil.roommate.entity.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/** Repositorio para la entidad {@link User}. */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

  /**
   * Busca un usuario por nombre de usuario.
   *
   * @param username Nombre de usuario.
   * @return Usuario encontrado, o null si no se encuentra.
   */
  User findByUsername(String username);

  /**
   * Obtiene una lista de todos los usuarios.
   *
   * @return Lista de {@link User}.
   */
  List<User> findAll();

  /**
   * Busca un usuario por ID.
   *
   * @param id ID del usuario.
   * @return Usuario encontrado, o null si no se encuentra.
   */
  Optional<User> findById(long id);

  /**
   * Elimina un usuario por ID.
   *
   * @param id ID del usuario a eliminar.
   */
  void deleteById(long id);

  /**
   * Verifica si existe un usuario con el nombre de usuario dado.
   *
   * @param username Nombre de usuario a verificar.
   * @return true si existe un usuario con el nombre de usuario dado, false de lo contrario.
   */
  boolean existsByUsername(String username);

  /**
   * Verifica si existen usuarios con el rol dado.
   *
   * @param role Rol a verificar.
   * @return true si existen usuarios con el rol dado, false de lo contrario.
   */
  @Query("SELECT COUNT(u) > 0 FROM User u WHERE u.role.name = :role")
  boolean existsByRole(@Param("role") String role);
}
