package com.isil.roommate.schedule;

import com.isil.roommate.service.BookingService;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/** Programador de tareas para cancelar reservas. */
@Component
public class BookingCancellationScheduler {

  @Autowired private BookingService bookingService;

  /** Cancela las reservas pendientes que tienen más de 24 horas de antigüedad. */
  @Scheduled(cron = "0 0 */24 * * *")
  public void cancelBookingsOlderThan24Hours() {
    LocalDateTime twentyFourHoursAgo = LocalDateTime.now().minusHours(24);
    bookingService.cancelPendingBookingsOlderThan(twentyFourHoursAgo);
  }
}
