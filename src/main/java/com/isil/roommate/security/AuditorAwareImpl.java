package com.isil.roommate.security;

import com.isil.roommate.entity.User;
import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Implementación de AuditorAware para obtener el nombre del usuario actual para la auditoría de
 * entidades.
 */
public class AuditorAwareImpl implements AuditorAware<String> {

  /**
   * Obtiene el nombre del usuario actual autenticado como auditor para las entidades auditables.
   *
   * @return Nombre del usuario actual o "00111111" si no hay autenticación.
   */
  @Override
  public Optional<String> getCurrentAuditor() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth == null || !auth.isAuthenticated()) {
      return Optional.of("00111111");
    }
    User user = (User) auth.getPrincipal();
    return Optional.of(user.getUsername());
  }
}
