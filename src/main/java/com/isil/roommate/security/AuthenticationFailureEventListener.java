package com.isil.roommate.security;

import com.isil.roommate.entity.User;
import com.isil.roommate.repository.ApplicationConfigurationRepository;
import com.isil.roommate.repository.UserRepository;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

/**
 * Escucha los eventos de fallo de autenticación para gestionar intentos de inicio de sesión
 * fallidos.
 */
@Component
public class AuthenticationFailureEventListener
    implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

  private final UserRepository userRepository;
  private final ApplicationConfigurationRepository applicationConfigurationRepository;

  @Autowired
  public AuthenticationFailureEventListener(
      UserRepository userRepository,
      ApplicationConfigurationRepository applicationConfigurationRepository) {
    this.userRepository = userRepository;
    this.applicationConfigurationRepository = applicationConfigurationRepository;
  }

  /**
   * Maneja el evento de fallo de autenticación.
   *
   * @param event El evento de fallo de autenticación.
   */
  @Override
  public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
    String username = event.getAuthentication().getPrincipal().toString();
    if (userRepository.existsByUsername(username)) {
      User user = userRepository.findByUsername(username);

      int maxLoginAttemptsThreshold = getMaxLoginAttemptsThreshold();
      int lockTimeDurationMinutes = getLockTimeDurationMinutes();

      if (user.getFailedAttempt() >= maxLoginAttemptsThreshold) {
        user.setFailedAttempt(0);
        user.setLockTime(LocalDateTime.now().plusMinutes(lockTimeDurationMinutes));
      } else {
        user.setFailedAttempt(user.getFailedAttempt() + 1);
      }
      userRepository.save(user);
    }
  }

  /**
   * Obtiene el umbral máximo de intentos de inicio de sesión permitidos.
   *
   * @return Umbral máximo de intentos de inicio de sesión permitidos.
   */
  private int getMaxLoginAttemptsThreshold() {
    String thresholdAsString =
        applicationConfigurationRepository.findByConfigKey("maxLoginAttemptsThreshold").getValue();
    return Integer.parseInt(thresholdAsString);
  }

  /**
   * Obtiene la duración del tiempo de bloqueo en minutos.
   *
   * @return Duración del tiempo de bloqueo en minutos.
   */
  private int getLockTimeDurationMinutes() {
    String durationAsString =
        applicationConfigurationRepository.findByConfigKey("lockTimeDurationMinutes").getValue();
    return Integer.parseInt(durationAsString);
  }
}
