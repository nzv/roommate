package com.isil.roommate.security;

import com.isil.roommate.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/** Configuración de seguridad para la aplicación. */
@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final UserDetailsServiceImpl userDetailsServiceImpl;

  /**
   * Constructor que recibe la implementación de UserDetailsServiceImpl.
   *
   * @param userDetailsServiceImpl Implementación de UserDetailsServiceImpl.
   */
  @Autowired
  public SecurityConfig(UserDetailsServiceImpl userDetailsServiceImpl) {
    this.userDetailsServiceImpl = userDetailsServiceImpl;
  }

  /**
   * Configuración de la seguridad HTTP.
   *
   * @param http Configuración HTTP.
   * @throws Exception Excepción en caso de error en la configuración.
   */
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.formLogin()
        .loginPage("/login")
        .permitAll()
        .and()
        .authorizeRequests()
        .requestMatchers(PathRequest.toStaticResources().atCommonLocations())
        .permitAll()
        .antMatchers("/**")
        .authenticated()
        .and()
        .logout()
        .logoutSuccessUrl("/login")
        .and()
        .csrf()
        .disable();
  }

  /**
   * Configuración del codificador de contraseñas.
   *
   * @return Instancia del codificador de contraseñas BCrypt.
   */
  @Bean
  public PasswordEncoder encoder() {
    return new BCryptPasswordEncoder();
  }

  /**
   * Configuración del administrador de autenticación.
   *
   * @param auth Administrador de autenticación.
   * @throws Exception Excepción en caso de error en la configuración.
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(encoder());
  }

  /**
   * Configuración del AuditorAware para la auditoría de entidades.
   *
   * @return Instancia de AuditorAwareImpl.
   */
  @Bean
  public AuditorAware<String> auditorAware() {
    return new AuditorAwareImpl();
  }
}
