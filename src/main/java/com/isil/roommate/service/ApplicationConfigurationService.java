package com.isil.roommate.service;

import com.isil.roommate.entity.ApplicationConfiguration;
import com.isil.roommate.exception.ApplicationConfigurationException;
import com.isil.roommate.repository.ApplicationConfigurationRepository;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Servicio para la gestión de configuraciones de la aplicación. */
@Service
public class ApplicationConfigurationService {

  private final EntityManager entityManager;
  private final ApplicationConfigurationRepository configurationRepository;

  @Autowired
  public ApplicationConfigurationService(
      EntityManager entityManager, ApplicationConfigurationRepository configurationRepository) {
    this.entityManager = entityManager;
    this.configurationRepository = configurationRepository;
  }

  /**
   * Obtiene la sesión actual de Hibernate.
   *
   * @return Sesión de Hibernate.
   */
  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }

  /** Habilita el filtro de entidades habilitadas. */
  private void enableEnabledFilter() {
    getSession().enableFilter("enabledFilter").setParameter("isEnabled", true);
  }

  /** Deshabilita el filtro de entidades habilitadas. */
  private void disableEnabledFilter() {
    getSession().disableFilter("enabledFilter");
  }

  /**
   * Obtiene todas las configuraciones de la aplicación.
   *
   * @return Lista de configuraciones de la aplicación.
   */
  public List<ApplicationConfiguration> findAll() {
    enableEnabledFilter();
    return configurationRepository.findAll();
  }

  /**
   * Guarda una configuración de la aplicación.
   *
   * @param configuration Configuración de la aplicación a guardar.
   * @throws ApplicationConfigurationException Si ocurre un error durante la operación.
   */
  public void save(ApplicationConfiguration configuration)
      throws ApplicationConfigurationException {
    disableEnabledFilter();
    try {
      configurationRepository.save(configuration);
    } catch (Exception e) {
      throw new ApplicationConfigurationException("Ocurrió un error inesperado: " + e.getMessage());
    }
  }

  /**
   * Guarda varias configuraciones de la aplicación.
   *
   * @param configurations Lista de configuraciones de la aplicación a guardar.
   * @throws ApplicationConfigurationException Si ocurre un error durante la operación.
   */
  public void saveAll(List<ApplicationConfiguration> configurations)
      throws ApplicationConfigurationException {
    disableEnabledFilter();
    try {
      configurationRepository.saveAll(configurations);
    } catch (Exception e) {
      throw new ApplicationConfigurationException("Ocurrió un error inesperado: " + e.getMessage());
    }
  }
}
