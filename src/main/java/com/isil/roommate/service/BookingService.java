package com.isil.roommate.service;

import com.isil.roommate.entity.Booking;
import com.isil.roommate.entity.BookingStatus;
import com.isil.roommate.exception.BookingOperationException;
import com.isil.roommate.repository.BookingRepository;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Servicio para la gestión de reservaciones. */
@Service
public class BookingService {

  private final EntityManager entityManager;
  private final BookingRepository bookingRepository;
  private final GuestService guestService;
  private final RoomService roomService;

  @Autowired
  public BookingService(
      EntityManager entityManager,
      BookingRepository bookingRepository,
      GuestService guestService,
      RoomService roomService) {
    this.entityManager = entityManager;
    this.bookingRepository = bookingRepository;
    this.guestService = guestService;
    this.roomService = roomService;
  }

  /**
   * Obtiene la sesión actual de Hibernate.
   *
   * @return Sesión de Hibernate.
   */
  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }

  /** Habilita el filtro de entidades habilitadas. */
  private void enableEnabledFilter() {
    getSession().enableFilter("enabledFilter").setParameter("isEnabled", true);
  }

  /** Deshabilita el filtro de entidades habilitadas. */
  private void disableEnabledFilter() {
    getSession().disableFilter("enabledFilter");
  }

  /**
   * Obtiene todas las reservaciones.
   *
   * @return Lista de reservaciones.
   */
  public List<Booking> findAll() {
    enableEnabledFilter();
    return bookingRepository.findAll();
  }

  /**
   * Obtiene una reservación por su ID.
   *
   * @param id ID de la reservación.
   * @return Reservación correspondiente al ID.
   * @throws BookingOperationException Si no se encuentra la reservación.
   */
  public Booking findById(long id) throws BookingOperationException {
    enableEnabledFilter();
    Booking booking =
        bookingRepository
            .findById(id)
            .orElseThrow(
                () -> new BookingOperationException("La reserva con el id '" + id + "' no existe"));
    return booking;
  }

  /**
   * Elimina una reservación por su ID.
   *
   * @param id ID de la reservación a eliminar.
   * @throws BookingOperationException Si no se encuentra la reservación o ocurre un error.
   */
  public void deleteById(long id) throws BookingOperationException {
    Booking booking = findById(id);
    delete(booking);
  }

  /**
   * Guarda una reservación.
   *
   * @param booking Reservación a guardar.
   * @throws BookingOperationException Si ocurre un error durante la operación.
   */
  public void save(Booking booking) throws BookingOperationException {
    disableEnabledFilter();
    try {
      booking.setRoom(roomService.findById(booking.getRoom().getId()));

      String guestDNI = booking.getGuest().getDni();
      if (booking.getGuest().getId() == null && !guestService.existsByDni(guestDNI)) {
        booking.setStatus(BookingStatus.PENDING);
        booking.setGuest(guestService.save(booking.getGuest()));
      } else {
        booking.setGuest(guestService.findByDni(guestDNI));
      }
      bookingRepository.save(booking);
    } catch (Exception e) {
      throw new BookingOperationException(
          "Ocurrió un error inesperado para reservaciones: " + e.getMessage());
    }
  }

  /**
   * Guarda varias reservaciones.
   *
   * @param bookings Lista de reservaciones a guardar.
   * @throws BookingOperationException Si ocurre un error durante la operación.
   */
  public void saveAll(List<Booking> bookings) throws BookingOperationException {
    disableEnabledFilter();
    try {
      bookingRepository.saveAll(bookings);
    } catch (Exception e) {
      throw new BookingOperationException(
          "Ocurrió un error inesperado para reservaciones: " + e.getMessage());
    }
  }

  /**
   * Elimina una reservación.
   *
   * @param booking Reservación a eliminar.
   * @throws BookingOperationException Si ocurre un error durante la operación.
   */
  public void delete(Booking booking) throws BookingOperationException {
    try {
      bookingRepository.delete(booking);
    } catch (Exception e) {
      throw new BookingOperationException(
          "Ocurrió un error inesperado para reservaciones: " + e.getMessage());
    }
  }

  /**
   * Obtiene el número total de reservaciones.
   *
   * @return Número total de reservaciones.
   */
  public long getCount() {
    enableEnabledFilter();
    return bookingRepository.count();
  }

  /**
   * Verifica si una habitación está reservada.
   *
   * @param roomId ID de la habitación.
   * @return true si la habitación está reservada, false de lo contrario.
   */
  public boolean isRoomBooked(Long roomId) {
    disableEnabledFilter();
    return bookingRepository.isRoomBooked(roomId);
  }

  /**
   * Cancela las reservaciones pendientes más antiguas que la marca de tiempo especificada.
   *
   * @param timestamp Marca de tiempo límite para las reservaciones pendientes.
   */
  public void cancelPendingBookingsOlderThan(LocalDateTime timestamp) {
    List<Booking> bookingsToCancel = bookingRepository.findPendingBookingsOlderThan(timestamp);
    for (Booking booking : bookingsToCancel) {
      booking.setStatus(BookingStatus.CANCELLED);
      bookingRepository.save(booking);
    }
  }
}
