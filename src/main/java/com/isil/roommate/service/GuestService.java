package com.isil.roommate.service;

import com.isil.roommate.entity.Guest;
import com.isil.roommate.exception.GuestOperationException;
import com.isil.roommate.repository.GuestRepository;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Servicio para la gestión de huéspedes. */
@Service
public class GuestService {

  private final EntityManager entityManager;
  private final GuestRepository guestRepository;

  @Autowired
  public GuestService(EntityManager entityManager, GuestRepository guestRepository) {
    this.entityManager = entityManager;
    this.guestRepository = guestRepository;
  }

  /**
   * Obtiene la sesión actual de Hibernate.
   *
   * @return Sesión de Hibernate.
   */
  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }

  /** Habilita el filtro de entidades habilitadas. */
  private void enableEnabledFilter() {
    getSession().enableFilter("enabledFilter").setParameter("isEnabled", true);
  }

  /** Deshabilita el filtro de entidades habilitadas. */
  private void disableEnabledFilter() {
    getSession().disableFilter("enabledFilter");
  }

  /**
   * Obtiene todos los huéspedes.
   *
   * @return Lista de huéspedes.
   */
  public List<Guest> findAll() {
    enableEnabledFilter();
    return guestRepository.findAll();
  }

  /**
   * Obtiene un huésped por su ID.
   *
   * @param id ID del huésped.
   * @return Huésped correspondiente al ID.
   * @throws GuestOperationException Si no se encuentra el huésped.
   */
  public Guest findById(long id) throws GuestOperationException {
    enableEnabledFilter();
    Guest guest =
        guestRepository
            .findById(id)
            .orElseThrow(
                () -> new GuestOperationException("El huésped con el id '" + id + "' no existe"));
    return guest;
  }

  /**
   * Obtiene un huésped por su DNI.
   *
   * @param dni DNI del huésped.
   * @return Huésped correspondiente al DNI.
   * @throws GuestOperationException Si no se encuentra el huésped.
   */
  public Guest findByDni(String dni) throws GuestOperationException {
    enableEnabledFilter();
    Guest guest =
        guestRepository
            .findByDni(dni)
            .orElseThrow(
                () -> new GuestOperationException("El huésped con el dni '" + dni + "' no existe"));
    return guest;
  }

  /**
   * Elimina un huésped por su ID.
   *
   * @param id ID del huésped a eliminar.
   * @throws GuestOperationException Si no se encuentra el huésped o ocurre un error.
   */
  public void deleteById(long id) throws GuestOperationException {
    Guest guest = findById(id);
    delete(guest);
  }

  /**
   * Guarda un huésped.
   *
   * @param guest Huésped a guardar.
   * @return Huésped guardado.
   * @throws GuestOperationException Si ocurre un error durante la operación.
   */
  public Guest save(Guest guest) throws GuestOperationException {
    disableEnabledFilter();
    try {
      return guestRepository.save(guest);
    } catch (Exception e) {
      throw new GuestOperationException(
          "Ocurrió un error inesperado para huéspedes: " + e.getMessage());
    }
  }

  /**
   * Guarda varios huéspedes.
   *
   * @param guests Lista de huéspedes a guardar.
   * @throws GuestOperationException Si ocurre un error durante la operación.
   */
  public void saveAll(List<Guest> guests) throws GuestOperationException {
    disableEnabledFilter();
    try {
      guestRepository.saveAll(guests);
    } catch (Exception e) {
      throw new GuestOperationException(
          "Ocurrió un error inesperado para huéspedes: " + e.getMessage());
    }
  }

  /**
   * Elimina un huésped.
   *
   * @param guest Huésped a eliminar.
   * @throws GuestOperationException Si ocurre un error durante la operación.
   */
  public void delete(Guest guest) throws GuestOperationException {
    try {
      guestRepository.delete(guest);
    } catch (Exception e) {
      throw new GuestOperationException(
          "Ocurrió un error inesperado para huéspedes: " + e.getMessage());
    }
  }

  /**
   * Verifica si existe un huésped con el DNI especificado.
   *
   * @param dni DNI del huésped a verificar.
   * @return true si existe un huésped con el DNI, false de lo contrario.
   */
  public boolean existsByDni(String dni) {
    disableEnabledFilter();
    return guestRepository.existsByDni(dni);
  }
}
