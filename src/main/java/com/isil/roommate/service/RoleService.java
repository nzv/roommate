package com.isil.roommate.service;

import com.isil.roommate.entity.Role;
import com.isil.roommate.exception.RoleOperationException;
import com.isil.roommate.repository.RoleRepository;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Servicio para la gestión de roles. */
@Service
public class RoleService {

  private final EntityManager entityManager;
  private final RoleRepository roleRepository;
  private final UserService userService;

  @Autowired
  public RoleService(
      EntityManager entityManager, RoleRepository roleRepository, UserService userService) {
    this.entityManager = entityManager;
    this.roleRepository = roleRepository;
    this.userService = userService;
  }

  /**
   * Obtiene la sesión actual de Hibernate.
   *
   * @return Sesión de Hibernate.
   */
  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }

  /** Habilita el filtro de entidades habilitadas. */
  private void enableEnabledFilter() {
    getSession().enableFilter("enabledFilter").setParameter("isEnabled", true);
  }

  /** Deshabilita el filtro de entidades habilitadas. */
  private void disableEnabledFilter() {
    getSession().disableFilter("enabledFilter");
  }

  /**
   * Obtiene todos los roles.
   *
   * @return Lista de roles.
   */
  public List<Role> findAll() {
    enableEnabledFilter();
    return roleRepository.findAll();
  }

  /**
   * Obtiene roles distintos.
   *
   * @return Lista de roles distintos.
   */
  public List<Role> findDistinctRoles() {
    enableEnabledFilter();
    return roleRepository.findDistinctRoles();
  }

  /**
   * Obtiene un rol por su ID.
   *
   * @param id ID del rol.
   * @return Rol correspondiente al ID.
   * @throws RoleOperationException Si no se encuentra el rol.
   */
  public Role findById(Long id) throws RoleOperationException {
    enableEnabledFilter();
    Role role =
        roleRepository
            .findById(id)
            .orElseThrow(
                () -> new RoleOperationException("El rol con el id '" + id + "' no existe"));
    return role;
  }

  /**
   * Guarda un rol.
   *
   * @param role Rol a guardar.
   * @return Rol guardado.
   * @throws RoleOperationException Si ocurre un error durante la operación.
   */
  public Role save(Role role) throws RoleOperationException {
    disableEnabledFilter();
    String name = role.getName();
    if (role.getId() == null && roleRepository.existsByName(name)) {
      throw new RoleOperationException("El rol '" + name + "' ya existe");
    }
    return roleRepository.save(role);
  }

  /**
   * Elimina un rol.
   *
   * @param role Rol a eliminar.
   * @throws RoleOperationException Si ocurre un error durante la operación.
   */
  public void delete(Role role) throws RoleOperationException {
    enableEnabledFilter();
    String roleName = role.getName();
    if (userService.existsByRole(roleName)) {
      throw new RoleOperationException(
          "No puedes eliminar el rol '" + roleName + "' porque aún tiene usuarios asignados");
    }
    roleRepository.delete(role);
  }

  /**
   * Elimina un rol por su ID.
   *
   * @param id ID del rol a eliminar.
   * @throws RoleOperationException Si no se encuentra el rol o ocurre un error.
   */
  public void deleteById(long id) throws RoleOperationException {
    Role role = findById(id);
    delete(role);
  }

  /**
   * Obtiene el número total de roles.
   *
   * @return Número total de roles.
   */
  public long getCount() {
    enableEnabledFilter();
    return roleRepository.count();
  }
}
