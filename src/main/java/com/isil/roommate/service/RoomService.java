package com.isil.roommate.service;

import com.isil.roommate.entity.BookingStatus;
import com.isil.roommate.entity.Room;
import com.isil.roommate.entity.RoomStatus;
import com.isil.roommate.exception.RoomOperationException;
import com.isil.roommate.repository.RoomRepository;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Servicio para la gestión de habitaciones. */
@Service
public class RoomService {

  private final EntityManager entityManager;
  private final RoomRepository roomRepository;

  @Autowired
  public RoomService(EntityManager entityManager, RoomRepository roomRepository) {
    this.entityManager = entityManager;
    this.roomRepository = roomRepository;
  }

  /**
   * Obtiene la sesión actual de Hibernate.
   *
   * @return Sesión de Hibernate.
   */
  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }

  /** Habilita el filtro de entidades habilitadas. */
  private void enableEnabledFilter() {
    getSession().enableFilter("enabledFilter").setParameter("isEnabled", true);
  }

  /** Deshabilita el filtro de entidades habilitadas. */
  private void disableEnabledFilter() {
    getSession().disableFilter("enabledFilter");
  }

  /**
   * Obtiene todas las habitaciones.
   *
   * @return Lista de habitaciones.
   */
  public List<Room> findAll() {
    enableEnabledFilter();
    return roomRepository.findAll();
  }

  /**
   * Obtiene una habitación por su ID.
   *
   * @param id ID de la habitación.
   * @return Habitación correspondiente al ID.
   * @throws RoomOperationException Si no se encuentra la habitación.
   */
  public Room findById(Long id) throws RoomOperationException {
    enableEnabledFilter();
    Room room =
        roomRepository
            .findById(id)
            .orElseThrow(
                () -> new RoomOperationException("La habitación con el id '" + id + "' no existe"));
    return room;
  }

  /**
   * Guarda una habitación.
   *
   * @param room Habitación a guardar.
   * @return Habitación guardada.
   * @throws RoomOperationException Si ocurre un error durante la operación.
   */
  public Room save(Room room) throws RoomOperationException {
    disableEnabledFilter();
    int number = room.getRoomNumber();
    if (room.getId() == null && roomRepository.existsByRoomNumber(number)) {
      throw new RoomOperationException("La habitación '" + number + "' ya existe");
    }
    return roomRepository.save(room);
  }

  /**
   * Elimina una habitación.
   *
   * @param room Habitación a eliminar.
   */
  public void delete(Room room) {
    enableEnabledFilter();
    roomRepository.delete(room);
  }

  /**
   * Elimina una habitación por su ID.
   *
   * @param id ID de la habitación a eliminar.
   * @throws RoomOperationException Si no se encuentra la habitación o ocurre un error.
   */
  public void deleteById(long id) throws RoomOperationException {
    enableEnabledFilter();
    Room room = findById(id);
    roomRepository.delete(room);
  }

  /**
   * Obtiene todas las habitaciones disponibles.
   *
   * @return Lista de habitaciones disponibles.
   */
  public List<Room> findAvailableRooms() {
    enableEnabledFilter();
    return roomRepository.findAvailableRooms(
        RoomStatus.CLEAN,
        List.of(BookingStatus.PENDING, BookingStatus.CONFIRMED, BookingStatus.CHECKED_IN));
  }

  /**
   * Obtiene el número total de habitaciones.
   *
   * @return Número total de habitaciones.
   */
  public long getCount() {
    enableEnabledFilter();
    return roomRepository.count();
  }
}
