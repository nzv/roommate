package com.isil.roommate.service;

import com.isil.roommate.entity.User;
import com.isil.roommate.exception.UserOperationException;
import com.isil.roommate.repository.RoleRepository;
import com.isil.roommate.repository.UserRepository;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Service;

/** Servicio para la gestión de usuarios. */
@Service
public class UserService {

  private final EntityManager entityManager;
  private final UserRepository userRepository;
  private final RoleRepository roleRepository;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public UserService(
      EntityManager entityManager,
      UserRepository userRepository,
      RoleRepository roleRepository,
      PasswordEncoder passwordEncoder) {
    this.entityManager = entityManager;
    this.userRepository = userRepository;
    this.roleRepository = roleRepository;
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * Obtiene la sesión actual de Hibernate.
   *
   * @return Sesión de Hibernate.
   */
  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }

  /** Habilita el filtro de entidades habilitadas. */
  private void enableEnabledFilter() {
    getSession().enableFilter("enabledFilter").setParameter("isEnabled", true);
  }

  /** Deshabilita el filtro de entidades habilitadas. */
  private void disableEnabledFilter() {
    getSession().disableFilter("enabledFilter");
  }

  /**
   * Verifica si existe un usuario con el nombre de usuario dado.
   *
   * @param username Nombre de usuario a verificar.
   * @return true si existe un usuario con el nombre de usuario dado, false en caso contrario.
   */
  public boolean existsByUsername(String username) {
    enableEnabledFilter();
    return userRepository.existsByUsername(username);
  }

  /**
   * Busca un usuario por su nombre de usuario.
   *
   * @param username Nombre de usuario a buscar.
   * @return Usuario correspondiente al nombre de usuario.
   */
  public User findByUsername(String username) {
    enableEnabledFilter();
    return userRepository.findByUsername(username);
  }

  /**
   * Obtiene todos los usuarios.
   *
   * @return Lista de usuarios.
   */
  public List<User> findAll() {
    enableEnabledFilter();
    return userRepository.findAll();
  }

  /**
   * Obtiene un usuario por su ID.
   *
   * @param id ID del usuario.
   * @return Usuario correspondiente al ID.
   * @throws UserOperationException Si no se encuentra el usuario.
   */
  public User findById(long id) throws UserOperationException {
    enableEnabledFilter();
    User user =
        userRepository
            .findById(id)
            .orElseThrow(
                () -> new UserOperationException("El usuario con el id '" + id + "' no existe"));
    return user;
  }

  /**
   * Elimina un usuario por su ID.
   *
   * @param aware Contexto de seguridad para verificar el usuario actual.
   * @param id ID del usuario a eliminar.
   * @throws UserOperationException Si no se encuentra el usuario o el usuario intenta eliminar su
   *     propia cuenta.
   */
  public void deleteById(SecurityContextHolderAwareRequestWrapper aware, long id)
      throws UserOperationException {
    User user = findById(id);
    delete(aware, user);
  }

  /**
   * Guarda un usuario.
   *
   * @param user Usuario a guardar.
   * @return Usuario guardado.
   * @throws UserOperationException Si ocurre un error durante la operación.
   */
  public User save(User user) throws UserOperationException {
    disableEnabledFilter();
    String pass = passwordEncoder.encode(user.getPassword());
    user.setPassword(pass);
    Long roleId = user.getRole().getId();
    user.setRole(roleRepository.findById(roleId).orElse(null));
    String name = user.getUsername();
    if (user.getId() == null && userRepository.existsByUsername(name)) {
      throw new UserOperationException("El usuario '" + name + "' ya existe");
    }
    return userRepository.save(user);
  }

  /**
   * Elimina un usuario.
   *
   * @param aware Contexto de seguridad para verificar el usuario actual.
   * @param user Usuario a eliminar.
   * @throws UserOperationException Si no se encuentra el usuario o el usuario intenta eliminar su
   *     propia cuenta.
   */
  public void delete(SecurityContextHolderAwareRequestWrapper aware, User user)
      throws UserOperationException {
    String name = user.getUsername();
    if (aware.getRemoteUser().equals(name)) {
      throw new UserOperationException("No puedes eliminar tu propio usuario");
    }
    userRepository.delete(user);
  }

  /**
   * Busca un usuario por su nombre de usuario sin aplicar el filtro de entidades habilitadas.
   *
   * @param username Nombre de usuario a buscar.
   * @return Usuario correspondiente al nombre de usuario.
   */
  public User forceFindByUsername(String username) {
    disableEnabledFilter();
    return userRepository.findByUsername(username);
  }

  /**
   * Obtiene el número total de usuarios.
   *
   * @return Número total de usuarios.
   */
  public long getCount() {
    enableEnabledFilter();
    return userRepository.count();
  }

  /**
   * Verifica si existen usuarios con un rol específico.
   *
   * @param role Rol a verificar.
   * @return true si existen usuarios con el rol dado, false en caso contrario.
   */
  public boolean existsByRole(String role) {
    enableEnabledFilter();
    return userRepository.existsByRole(role);
  }
}
